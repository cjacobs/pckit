package accounts;

import java.security.SecureRandom;
import java.math.BigInteger;
import java.io.*;
import java.util.*;
import java.sql.*;

import store.servlets.CartManagerUtil;

//Code by Clay Jacobs
/**
 * This class provides methods to perform database queries affecting UserLogin objects
 * @author Clay Jacobs
 */
public class LoginTracker {

   /**
    * a CartManagerUtil object
    */
   private CartManagerUtil cmUtil; 

   /**
    * Constructor
    */
   public LoginTracker() {
      cmUtil= new CartManagerUtil();
   }
   
   /**
    * @param timestamp a date as a java.sql.Timestamp
    * @return the java.util.Date representation of timestamp
    */
   public java.util.Date toDate(java.sql.Timestamp timestamp) {
        long milliseconds = timestamp.getTime() + (timestamp.getNanos() / 1000000);
        return new java.util.Date(milliseconds);
    }
    
    /**
     * @param date a date as a java.util.Date
     * @return the java.sql.Timestamp representation of date
     */
    public java.sql.Timestamp toTimestamp(java.util.Date date) {
        return new java.sql.Timestamp(date.getTime());
    }
    
    /** get all active user logins
     *
     * @param now the current date
     * @param conn the database connection to use for queries
     * @return the ArrayList of all active UserLogins
     */
    public ArrayList<UserLogin> getAllLogins(java.util.Date now, Connection conn) throws Exception {
       ArrayList<UserLogin> logins = new ArrayList<UserLogin>();
       
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "SELECT * FROM PCKitSessions";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       ResultSet rs = pstatement.executeQuery();
       while(rs.next()) {
          boolean loggedIn = rs.getInt("isLogged") == 1;
          UserLogin login = new UserLogin(rs.getString("sessionId"), rs.getInt("userId"), toDate(rs.getTimestamp("sessionStart")), rs.getInt("length"));
          login.setLoggedIn(loggedIn);
          logins.add(login);
       }
       rs.close();
       pstatement.close();
       
       return logins;
    }
   
   /** expire any user logins that have timed out.
    *
    * @param now the current date
    * @param conn the database connection to use for queries
    */
   public void refreshAllLogins(java.util.Date now, Connection conn) throws Exception {
       ArrayList<UserLogin> logins = new ArrayList<UserLogin>();
       
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "SELECT * FROM PCKitSessions";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       ResultSet rs = pstatement.executeQuery();
       while(rs.next()) {
          boolean loggedIn = rs.getInt("isLogged") == 1;
          UserLogin login = new UserLogin(rs.getString("sessionId"), rs.getInt("userId"), toDate(rs.getTimestamp("sessionStart")), rs.getInt("length"));
          login.setLoggedIn(loggedIn);
          logins.add(login);
       }
       rs.close();
       pstatement.close();
       
       
       for (int i=0; i<logins.size(); i++) {
          UserLogin login = logins.get(i);
          
          if (!(login.isAlive(now))) {
             logoutUserSession(login.getSessionId(), conn);
          }
       }
   }
   
   
   /** generate an unused secure random session id to associate with a new login
    *
    * @param now the current date
    * @param conn the database connection to use for queries
    * @return the generated session id
    */
   public String nextSessionId(java.util.Date now, Connection conn) throws Exception  {
       
       
       String result = null;
       while (result == null) {
          SecureRandom random = new SecureRandom();
          String curVal = new BigInteger(130, random).toString(32);
          if (!sessionIdInUse(curVal, now, conn)) {
             result = curVal;
          }
       }
       
       return result;  
    }
    
    /*Logout current  sessionId */
    /** logout the specified session id, regardless of expirationDate
     *
     * @param sessionId id of the session to logout
     * @param conn the database connection to use for queries
     */
    public void logoutUserSession(String sessionId, Connection conn) throws Exception {
       
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "DELETE FROM PCKitSessions WHERE sessionId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setString(1, sessionId);
       pstatement.executeUpdate();
       pstatement.close();
       
       String queryString2 = "DELETE FROM PCKitSessionData WHERE sessionId=?";
       PreparedStatement pstatement2 = conn.prepareStatement(queryString2);
       pstatement2.setString(1, sessionId);
       pstatement2.executeUpdate();
       pstatement2.close();
       
    }
    
    
    /*Get user login for current user */
    /** construct the UserLogin object for the specified session id
     *
     * @param sessionId id of the session to get data for
     * @param conn the database connection to use for queries
     * @return UserLogin object for the current session
     */
    public UserLogin getUserInfo(String sessionId, Connection conn)  throws Exception {
       String queryString = "SELECT * FROM PCKitSessions WHERE sessionId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setString(1, sessionId);
       ResultSet rs = pstatement.executeQuery();
       rs.next();
       int userId = rs.getInt("userId");
       
       
       UserLogin login = new UserLogin(rs.getString("sessionId"), rs.getInt("userId"), toDate(rs.getTimestamp("sessionStart")), rs.getInt("length"));
       login.setLoggedIn(true);
       
       rs.close();
       pstatement.close();
       
       String queryString2 = "SELECT * FROM PCKitAccounts WHERE userId=?";
       PreparedStatement pstatement2 = conn.prepareStatement(queryString2);
       pstatement2.setInt(1, userId);
       ResultSet rs2 = pstatement2.executeQuery();
       rs2.next();
       String firstName = rs2.getString("firstName");
       String lastName =rs2.getString("lastName");
       String email= rs2.getString("email");
       rs2.close();
       pstatement2.close();
       
       String queryString3 = "SELECT * FROM PCKitSessionData WHERE sessionId=? and userId=?";
       PreparedStatement pstatement3 = conn.prepareStatement(queryString3);
       pstatement3.setString(1, sessionId);
       pstatement3.setInt(2, userId);
       ResultSet rs3 = pstatement3.executeQuery();
       rs3.next();
       int activeOrderId = rs3.getInt("activeOrderId");
       rs3.close();
       pstatement3.close();
       
       
       login.setFirstName(firstName);
       login.setLastName(lastName);
       login.setEmail(email);
       login.setOrders(cmUtil.getUserCarts(userId, conn));
       login.setActiveCart(activeOrderId);
       return login;
    }
    
    /*Logout all sessions with sessionId */
    /*public void logoutSessions(String sessionId, Connection conn) throws Exception {
       ArrayList<String> updateSessions
       Connection connection = null;
       PreparedStatement pstatement = null;
       ResultSet rs =null;
       
       PreparedStatement pstatement2 = null;
       int 
    }*/
    
    /*Check if generated session id is already in the db and is active */
    /** Checks if generated session id is already in the db and is active
     *
     * @param sessionId id of the session to check
     * @param now the current date
     * @param conn the database connection to use for queries
     * @return true if session exists and is active. False otherwise.
     */
    public boolean sessionIdInUse(String sessionId, java.util.Date now, Connection conn) throws Exception {
       boolean sessionIdFound = false;
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "SELECT * FROM PCKitSessions WHERE sessionId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setString(1, sessionId);
       ResultSet rs = pstatement.executeQuery();
       while (rs.next()) {
          if (!sessionIdFound) {
             int loggedStatus = rs.getInt("isLogged");
             java.util.Date start = toDate(rs.getTimestamp("sessionStart"));
             Calendar cal = Calendar.getInstance();
             cal.setTime(start);
             cal.add(Calendar.MINUTE, rs.getInt("length"));
             java.util.Date end = cal.getTime();
             
             if (now.compareTo(start) >= 0 && now.compareTo(end) <= 0 && loggedStatus==1) {
                sessionIdFound=true;
             }
          }
       }
       rs.close();
       pstatement.close();
       
       return sessionIdFound;
       
    }
    
    /*Check if user session timed out*/
    /** Checks if user session timed out
     *
     * @param sessionId id of the session to check
     * @param now the current date
     * @param conn the database connection to use for queries
     * @return true if session has expired. False otherwise
     */
    public boolean userLoginExpired(String sessionId, java.util.Date now, Connection conn) throws Exception {
       boolean expired = false;
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "SELECT * FROM PCKitSessions WHERE sessionId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setString(1, sessionId);
       ResultSet rs = pstatement.executeQuery();
       rs.next();
       int loggedStatus = rs.getInt("isLogged");
       java.util.Date start = toDate(rs.getTimestamp("sessionStart"));
       Calendar cal = Calendar.getInstance();
       cal.setTime(start);
       cal.add(Calendar.MINUTE, rs.getInt("length"));
       java.util.Date end = cal.getTime();
       
       if (now.compareTo(start) < 0 || now.compareTo(end) > 0) {
          expired =true;
       }
       rs.close();
       pstatement.close();
       
       return expired;
    }
    
    /*Check if user login exists on password entry*/
    /** Check if user login exists on password entry
     *
     * @param userId id of the user to check
     * @param conn the database connection to use for queries
     * @return true if session exists. False otherwise
     */
    public boolean userLoginExists(int userId, Connection conn) throws Exception {
       int rowCount =-1;
    
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "SELECT COUNT(*) FROM PCKitSessions WHERE userId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setInt(1, userId);
       ResultSet rs = pstatement.executeQuery();
       rs.next();
       rowCount = rs.getInt(1);
       
       rs.close();
       pstatement.close();
       return rowCount > 0;
       
    }
    
    /*Check if user is logged in on password entry*/
    /** Check if user with specified id is logged in
     *
     * @param userId id of the user to check
     * @param conn the database connection to use for queries
     * @return true if user with userId is logged in. False otherwise
     */
    public boolean userLoggedIn(int userId, Connection conn) throws Exception {
       boolean loggedIn = false;
    
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "SELECT * FROM PCKitSessions WHERE userId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setInt(1, userId);
       
       
       ResultSet rs = pstatement.executeQuery();
       while(rs.next()) {
          loggedIn = rs.getInt("isLogged") == 1;
       }
       rs.close();
       pstatement.close();
     
       return loggedIn;  
    }
    
    
    
    
    /*Create user login on password entry*/
    /** Create user login in db on password entry
     *
     * @param userId id of the user who is logging in
     * @param sessionId id of the session to create for this login
     * @param now the current date
     * @param length time until this login expires
     * @param conn the database connection to use for queries
     * @return a UserLogin object for this login
     */
    public UserLogin createLogin(int userId, String sessionId, java.util.Date now, int length, Connection conn)  throws Exception {
       
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "INSERT INTO PCKitSessions(sessionId, userId, sessionStart, length, isLogged) VALUES (?, ?, ?, ?, ?)";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setString(1, sessionId);
       pstatement.setInt(2, userId);
       pstatement.setTimestamp(3, toTimestamp(now));
       pstatement.setInt(4, length);
       pstatement.setInt(5, 1);
       pstatement.executeUpdate();
       pstatement.close();
       
       
       String queryString2 = "SELECT * FROM PCKitAccounts WHERE userId=?";
       PreparedStatement pstatement2 = conn.prepareStatement(queryString2);
       pstatement2.setInt(1, userId);
       ResultSet rs = pstatement2.executeQuery();
       rs.next();
       String firstName = rs.getString("firstName");
       String lastName =rs.getString("lastName");
       String email= rs.getString("email");
       rs.close();
       pstatement2.close();
       
       String queryString3 = "INSERT INTO PCKitSessionData(sessionId, userId, activeOrderId) VALUES (?, ?, ?)";
       PreparedStatement pstatement3 = conn.prepareStatement(queryString3);
       pstatement3.setString(1, sessionId);
       pstatement3.setInt(2, userId);
       pstatement3.setNull(3, java.sql.Types.INTEGER);
       pstatement3.executeUpdate();
       pstatement3.close();
       
       UserLogin login = new UserLogin(sessionId, userId, now, length);
       login.setLoggedIn(true);
       login.setFirstName(firstName);
       login.setLastName(lastName);
       login.setEmail(email);
       login.setOrders(cmUtil.getUserCarts(userId, conn));
       return login;
       
    }
    
    
    /*Update user login on password entry in db*/
    /** Update session data for a user's login in db
     *
     * @param userId id of the user owning a session that should be updated
     * @param sessionId id of the session to set for this login
     * @param now the current date
     * @param length time until this login expires
     * @param conn the database connection to use for queries
     * @return a UserLogin object for this login
     */
    public UserLogin updateLogin(int userId, String sessionId, java.util.Date now, int length, Connection conn)  throws Exception {
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "UPDATE PCKitSessions SET sessionId=?, sessionStart=?, length=?, isLogged=? WHERE userId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setString(1, sessionId);
       pstatement.setTimestamp(2, toTimestamp(now));
       pstatement.setInt(3, length);
       pstatement.setInt(4, 1);
       pstatement.setInt(5, userId);
       pstatement.executeUpdate();
       pstatement.close();
       
       String queryString2 = "SELECT * FROM PCKitAccounts WHERE userId=?";
       PreparedStatement pstatement2 = conn.prepareStatement(queryString2);
       pstatement2.setInt(1, userId);
       ResultSet rs = pstatement2.executeQuery();
       rs.next();
       String firstName = rs.getString("firstName");
       String lastName =rs.getString("lastName");
       String email= rs.getString("email");
       rs.close();
       pstatement2.close();
       
       UserLogin login = new UserLogin(sessionId, userId, now, length);
       login.setLoggedIn(true);
       login.setFirstName(firstName);
       login.setLastName(lastName);
       login.setEmail(email);
       login.setOrders(cmUtil.getUserCarts(userId, conn));
       return login;
    }
    
    /** Update the active order id to be used with this session
     *
     * @param userId id of the user owning the session
     * @param sessionId id of the session for this login
     * @param orderId id of the order to set as active for this user.
     * @param conn the database connection to use for queries
     */
    public void updateActiveOrderId(int userId, String sessionId, int orderId, Connection conn)  throws Exception {
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "UPDATE PCKitSessionData SET activeOrderId=? WHERE sessionId=? and userId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setInt(1, orderId);
       pstatement.setString(2, sessionId);
       pstatement.setInt(3, userId);
       pstatement.executeUpdate();
       pstatement.close();
    }
    
    /** Set all occurrences of the specified active order id to null
     *
     * @param orderId id of the active order to search for.
     * @param conn the database connection to use for queries
     */
    public void clearActiveOrderId(int orderId,  Connection conn)  throws Exception {
       ArrayList<String> sessionIds = new ArrayList<String>();
    
       Class.forName("com.mysql.jdbc.Driver");
       String queryString = "Select * from PCKitSessionData WHERE activeOrderId=?";
       PreparedStatement pstatement = conn.prepareStatement(queryString);
       pstatement.setInt(1, orderId);
       ResultSet rs = pstatement.executeQuery();
       while(rs.next()) {
          String sessionId = rs.getString("sessionId");
          sessionIds.add(sessionId);
       }
       
       rs.close();
       pstatement.close();
       
       for (int i=0; i<sessionIds.size(); i++) {
          String queryString2 = "UPDATE PCKitSessionData SET activeOrderId=? WHERE sessionId=?";
          PreparedStatement pstatement2 = conn.prepareStatement(queryString2);
          pstatement2.setNull(1, java.sql.Types.INTEGER);
          pstatement2.setString(2, sessionIds.get(i));
          pstatement2.executeUpdate();
          pstatement2.close();
       }
       
    }
    
    
    
    
    

}