package accounts;

import javax.servlet.http.HttpServletRequest;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;

import java.sql.*;
import java.net.URLEncoder;
import java.util.*;

import java.util.StringTokenizer;    
import util.SecureEncrypt;

//Code by Clay Jacobs
 
 /**
 * This class provides methods to perform overall user authorization and code security related functions
 * @author Clay Jacobs
 */
public class AuthJWTUtil {

    /**
     * key used for securing jwt content. Should change if comprimised.
     */
    private String jwtKey = "J+tPyNi6T2KP4cHF/Z1HTGCsWQutvAe2cQDUqcTgheU=";
    /**
     * a SecureEncrypt object
     */
    private SecureEncrypt seTest;
    /**
     * a LoginTracker object
     */
    private LoginTracker loginTracker;
    
    /**
     * a UserLogin object for storing a result
     */
    private UserLogin userLogin;
    /**
     * a String object for storing a result
     */
    private String outputToken;

    /**
     * Constructor
     */
    public AuthJWTUtil() {
       seTest = new SecureEncrypt();
       loginTracker = new LoginTracker();
       userLogin =null;
       outputToken = null;
    }
    
    /** get (device specific) ip address associated with a page request. This needs further testing.
     * 
     * @param request the HttpServletRequest object
     * @return the ip address 
     */
    public String getUserIP(HttpServletRequest request) {
        String ip = request.getHeader("X-Pounded-For");

		if (ip != null) {
			return ip;
		}

        ip = request.getHeader("x-forwarded-for");

        if (ip == null) {
        	return request.getRemoteAddr();
        }
        else {
        	// Process the IP to keep the last IP (real ip of the computer on the net)
            StringTokenizer tokenizer = new StringTokenizer(ip, ",");

            // Ignore all tokens, except the last one
            for (int i = 0; i < tokenizer.countTokens() -1 ; i++) {
            	tokenizer.nextElement();
            }

            ip = tokenizer.nextToken().trim();

            if (ip.equals("")) {
            	ip = null;
            }
        }

        // If the ip is still null, we put 0.0.0.0 to avoid null values
        if (ip == null) {
        	ip = "0.0.0.0";
        }

        return ip;
    }
    
    /** create JWT for accessing a login
     * 
     * @param id the id to use for the jwt (session id in this case)
     * @param issuer the issuer of the jwt
     * @param subject the subject of the jwt
     * @param nowMillis the date this jwt/session was created in milliseconds
     * @param ttlMillis the lifetime of this jwt/session in milliseconds
     * @return the jwt for accessing the user login 
     */
    public String createJWT(String id, String issuer, String subject, long nowMillis, long ttlMillis) throws Exception {
       //The JWT signature algorithm we will be using to sign the token
       //SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
       
       //long nowMillis = System.currentTimeMillis();
       java.util.Date now = new java.util.Date(nowMillis);
       
       //We will sign our JWT with our jwtKey secret
       byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(jwtKey);
       Key signingKey = new SecretKeySpec(apiKeySecretBytes, "HmacSHA256");
       
       
       JwtBuilder builder = Jwts.builder();
       builder.setId(id).setIssuedAt(now).setSubject(subject).setIssuer(issuer).setNotBefore(now).claim("test", "blah").signWith(SignatureAlgorithm.HS256, signingKey);
            
            //.setNotBefore(now).claim("test", "Hello")                    
        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
           long expMillis = nowMillis + ttlMillis;
           java.util.Date exp = new java.util.Date(expMillis);
           builder.setExpiration(exp);
        }
        
        //Builds the JWT and serializes it to a compact, URL-safe string
        return seTest.encryptToString(builder.compact(), "AES");
    
    }
    
    /** parse data out of JWT. Should throw exception if jwt is tampered.
     * 
     * @param jwt content to be deciphered
     * @return the deciphered jwt content
     */
    public Claims parseJWT(String jwt) throws Exception {
       //This line will throw an exception if it is not a signed JWS (as expected)
       //System.out.println("Encrypted jwt: " + jwt);
       String rawJwt = seTest.decryptToString(jwt, "AES");
       //System.out.println("Raw jwt: " + rawJwt);
       Claims claims = Jwts.parser()         
         .setSigningKey(DatatypeConverter.parseBase64Binary(jwtKey))
         .parseClaimsJws(rawJwt).getBody();
       
       /*System.out.println("ID: " + claims.getId());
       System.out.println("Subject: " + claims.getSubject());
       System.out.println("Issuer: " + claims.getIssuer());
       System.out.println("Expiration: " + claims.getExpiration());
       System.out.println("test: " + claims.get("test", String.class));*/
       return claims;
    }
    
    /** validate that existing JWT is still a valid user login. Store login result in userLogin.
     * 
     * @param token jwt content to be deciphered for validation
     * @param now the current date
     * @param conn the database connection to use for queries
     * @return "Valid" if UserLogin is still valid. "Reload" if not. 
     */
    public String validateToken(String token, java.util.Date now, Connection conn) throws Exception {
        // Check if it was issued by the server and if it's not expired
        // Throw an Exception if the token is invalid
        String result = "Validation error";
        Claims claims = parseJWT(token);
        String sessionId  = claims.getId();
        if (!loginTracker.userLoginExpired(sessionId, now, conn)) {
           
           userLogin = loginTracker.getUserInfo(sessionId, conn);
           result = "Valid";
        }
        else {
           loginTracker.logoutUserSession(sessionId, conn);
           result = "Reload";
        }
        
        return result;
    }
    
    /** setup a new user login and it's associated JWT
     * 
     * @param userId the id of the user who we are loggin in
     * @param nowMillis the current date in milliseconds
     * @param mins the lifetime of the user login in minutes
     * @param conn the database connection to use for queries 
     */
    public void authorize(int userId, long nowMillis, int mins, Connection conn) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
        String result = "Validation error";
        java.util.Date now = new java.util.Date(nowMillis);
        long lengthMillis = ((long) mins)* 60 * 1000;
        String curSessionId = loginTracker.nextSessionId(now, conn);
        userLogin = loginTracker.createLogin(userId, curSessionId, now, mins, conn);
        outputToken =createJWT(curSessionId,"https://www.pckit.org", "PCKitData", nowMillis, lengthMillis);
        
        
        
    }
    
    /** logout the user login for the given JWT
     * 
     * @param token jwt content to be deciphered for deauthorization
     * @param conn the database connection to use for queries 
     */
    public void deauthorize(String token, Connection conn) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
        Claims claims = parseJWT(token);
        String sessionId  = claims.getId();
        loginTracker.logoutUserSession(sessionId, conn);
        
    }
    
    /** refresh state of all userLogins when this is called so we can clear out expired logins
     * 
     * @param now the current date
     * @param conn the database connection to use for queries 
     */
    public void refreshAll(java.util.Date now, Connection conn)  throws Exception {
        loginTracker.refreshAllLogins(now, conn);
    }
    
    /** get all user logins that are currently active
     * 
     * @param now the current date
     * @param conn the database connection to use for queries 
     * @return an ArrayList of all active UserLogin
     */
    public ArrayList<UserLogin> getAll(java.util.Date now, Connection conn)  throws Exception {
        return loginTracker.getAllLogins(now, conn);
    }

    /** get stored UserLogin result
     *  
     * @return userLogin
     */
    public UserLogin getLoginResult() {
        return userLogin;
    }
    
    /** get stored JWT result
     *  
     * @return outputToken
     */
    public String getJWTResult() {
        return outputToken;
    }
    
	/*login db methods*/
	/** Update the active order id to be used with this session
     *
     * @param userId id of the user owning the session
     * @param sessionId id of the session for this login
     * @param orderId id of the order to set as active for this user.
     * @param conn the database connection to use for queries
     */
	public void setOrderId(int userId, String sessionId, int orderId, Connection conn)  throws Exception {
        loginTracker.updateActiveOrderId(userId, sessionId, orderId, conn);
    }
    
    /** Set all occurrences of the specified active order id to null
     *
     * @param orderId id of the active order to search for.
     * @param conn the database connection to use for queries
     */
    public void deleteOrderId(int orderId, Connection conn)  throws Exception {
        loginTracker.clearActiveOrderId(orderId, conn);
    }
    
    /*CSRF cookie methods*/
    /** create JWT for csrf token
     * 
     * @param id the id to use for the jwt (csrf token in this case)
     * @param issuer the issuer of the jwt
     * @param subject the subject of the jwt
     * @param nowMillis the date this jwt was created in milliseconds
     * @param ttlMillis the lifetime of this jwt in milliseconds
     * @param csrf value of csrf token
     * @return the jwt for accessing the csrf token 
     */
    public String createJWTForCSRF(String id, String issuer, String subject, long nowMillis, long ttlMillis, String csrf) throws Exception {
       //The JWT signature algorithm we will be using to sign the token
       //SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
       
       //long nowMillis = System.currentTimeMillis();
       java.util.Date now = new java.util.Date(nowMillis);
       
       //We will sign our JWT with our jwtKey secret
       byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(jwtKey);
       Key signingKey = new SecretKeySpec(apiKeySecretBytes, "HmacSHA256");
       
       
       JwtBuilder builder = Jwts.builder();
       builder.setId(id).setIssuedAt(now).setSubject(subject).setIssuer(issuer).setNotBefore(now).claim("csrf", csrf).signWith(SignatureAlgorithm.HS256, signingKey);
            
            //.setNotBefore(now).claim("test", "Hello")                    
        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
           long expMillis = nowMillis + ttlMillis;
           java.util.Date exp = new java.util.Date(expMillis);
           builder.setExpiration(exp);
        }
        
        //Builds the JWT and serializes it to a compact, URL-safe string
        return seTest.encryptToString(builder.compact(), "AES");
    
    }
    
    /** setup a new JWT for a csrf token
     * 
     * @param csrfValue the csrf token value
     * @param nowMillis the current date in milliseconds
     * @param mins the lifetime of the user login in minutes
     * @return the jwt for accessing the csrf token
     */
    public String makeCSRFCookie(String csrfValue, long nowMillis, int mins) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
        java.util.Date now = new java.util.Date(nowMillis);
        long lengthMillis = ((long) mins)* 60 * 1000;
        return createJWTForCSRF(csrfValue, "https://www.pckit.org", "PCKitCSRF", nowMillis, lengthMillis, csrfValue);

    }
    
    /** get the csrf token value from the jwt
     * 
     * @param jwt the jwt content to be deciphered
     * @return the csrf token value
     */
    public String extractCSRF(String jwt) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
        //String result = "Validation error";
        Claims claims = parseJWT(jwt);
        String csrf = claims.get("csrf", String.class);
        return csrf;
        
        
    }
	
	/*HTML escape String*/
	/** sanitize string to display in page html
     * 
     * @param input the unsanitized text to be handled
     * @return the sanitized text to display in page
     */
	public String escapeHTML(String input) {
	    char[] unsafeChars = {'<', '>','&','"','\'', '/'};
	    String[] replacements = {"&lt;", "&gt;", "&amp;", "&quot;", "&#x27", "&#x2F"};
	    
	    String escapedText= "";
	    char tmp = ' ';
	    for (int i=0; i<input.length(); i++) {
	       tmp = input.charAt(i);
	       int found = -1;
	       for (int j=0; j<unsafeChars.length && found == -1; j++) {
	          if (tmp == unsafeChars[j]) {
	             found = j;
	          }
	       }
	       
	       if (found != -1) {
	          escapedText=escapedText + replacements[found];
	       }
	       else {
	          escapedText=escapedText + tmp;
	       }
	    }
	    return escapedText;
	}
	
}
