package accounts;

import java.util.Date;
import java.util.*;
import store.cart.ShoppingCart;

//Code by Clay Jacobs
/**
 * Model class to represent a user login.
 * @author Clay Jacobs
 */
public class UserLogin {

   /*Login data*/
   /**
    * the session id for this login
    */
   private String sessionId;
   /**
    * the session ip address of this login. Not currently being used. Could be implemented if desired.
    */
   private String sessionIP;
   /**
    * the user id associated with this login
    */
   private int userId;
   /**
    * the date when this user logged in
    */
   private Date startDate;
   /**
    * the date when this user login expires
    */
   private Date endDate;
   /**
    * whether the login is still active
    */
   private boolean loggedIn;
   
   /*User data*/
   /**
    * the user's first name
    */
   private String firstName;
   /**
    * the user's last name
    */
   private String lastName;
   /**
    * the user's email
    */
   private String email;
   
   /*Order data*/
   /**
    * the user's active cart
    */
   private ShoppingCart activeCart;
   /**
    * all the user's carts (both active and completed orders)
    */
   private ArrayList<ShoppingCart> orders;
   
   /**
    * Empty constructor. Use setters to initialize the other properties
    */
   public UserLogin() {
      orders = new ArrayList<ShoppingCart>();
   }
   
   /**
    * Constructor. Use if ips are implemented.
    */
   public UserLogin(String sId, String ip, int uId, Date start, int length) {
      sessionId = sId;
      sessionIP = ip;
      userId = uId;
      startDate = start;
      Calendar cal = Calendar.getInstance();
      cal.setTime(start);
      cal.add(Calendar.MINUTE, length);
      endDate = cal.getTime();
      orders = new ArrayList<ShoppingCart>();
      activeCart = null;
   }
   
   /**
    * Constructor. Use if ips aren't implemented.
    */
    public UserLogin(String sId, int uId, Date start, int length) {
      sessionId = sId;
      sessionIP = null;
      userId = uId;
      startDate = start;
      Calendar cal = Calendar.getInstance();
      cal.setTime(start);
      cal.add(Calendar.MINUTE, length);
      endDate = cal.getTime();
      orders = new ArrayList<ShoppingCart>();
      activeCart = null;
   }
   
   
   /*login data methods*/
   /** gets the session id
    *
    * @return the sessionId
    */
   public String getSessionId() {
      return sessionId;
   }
   
   /** sets the session id
    *
    * @param sId the value to use for sessionId
    */
   public void setSessionId(String sId) {
      sessionId=sId;
   }
   
   /** gets the session ip
    *
    * @return the sessionIP
    */
   public String getSessionIP() {
      return sessionIP;
   }
   
   /** sets the session ip
    *
    * @param ip the value to use for sessionIP
    */
   public void setSessionIP(String ip) {
      sessionIP=ip;
   }
   
   /** gets the user id
    *
    * @return the userId
    */
   public int getUserId() {
      return userId;
   }
   
   /** sets the user id
    *
    * @param uId the value to use for userId
    */
   public void setUserId(int uId) {
      userId=uId;
   }
   
   /** gets the start date
    *
    * @return the startDate
    */
   public Date getStartDate() {
      return startDate;
   }
   
   /** sets the start date
    *
    * @param start the value to use for startDate
    */
   public void setStartDate(Date start) {
      startDate=start;
   }
   
   /** gets the end date
    *
    * @return the endDate
    */
   public Date getEndDate() {
      return endDate;
   }
   
   /** sets the end date
    *
    * @param end the value to use for endDate
    */
   public void setEndDate(Date end) {
      endDate=end;
   }
   
   /** gets the logged in status
    *
    * @return loggedIn
    */
   public boolean getLoggedIn() {
      return loggedIn;
   }
   
   /** sets the logged in status
    *
    * @param val the value to use for loggedIn
    */
   public void setLoggedIn(boolean val) {
      loggedIn=val;
   }
   
   /** checks whether login is still active
    *
    * @param now the current Date
    * @return true if now is between startDate and endDate. False otherwise.
    */
   public boolean isAlive(Date now) {
      if (now.compareTo(startDate) < 0 || now.compareTo(endDate) > 0) {
         return false;
      }
      return true;
   }
   
   
   /*user data methods*/
   /** gets the user's first name 
    *
    * @return firstName
    */
   public String getFirstName() {
      return firstName;
   }
   
   /** sets the user's first name 
    *
    * @param first the value to use for firstName
    */
   public void setFirstName(String first) {
      firstName=first;
   }
   
   /** gets the user's last name 
    *
    * @return lastName
    */
   public String getLastName() {
      return lastName;
   }
   
   /** sets the user's last name 
    *
    * @param last the value to use for lastName
    */
   public void setLastName(String last) {
      lastName=last;
   }
   
   /** gets the user's email
    *
    * @return email
    */
   public String getEmail() {
      return email;
   }
   
   /** sets the user's email
    *
    * @param value the value to use for email
    */
   public void setEmail(String value) {
      email=value;
   }
   
   
   /*order data methods*/
   /** gets all the user's orders
    *
    * @return orders
    */
   public ArrayList<ShoppingCart> getOrders() {
      return orders;
   }
   
   /** sets all the user's orders
    *
    * @param allOrders the ArrayList of ShoppingCart objects to use for orders
    */
   public void setOrders(ArrayList<ShoppingCart> allOrders) {
      orders=allOrders;
   }
   
   /** gets the user's active ShoppingCart
    *
    * @return activeCart
    */
   public ShoppingCart getActiveCart() {
      return activeCart;
   }
   
   /** sets the user's active ShoppingCart
    *
    * @param orderId the id of the ShoppingCart to use as the activeCart
    */
   public void setActiveCart(int orderId) {
      activeCart=this.find(orderId);
   }
   
   /** finds the order with the given orderId
    *
    * @param orderId the id of the order to search for
    * @return the ShoppingCart with the given orderId, or null if none is found for this user.
    */
   public ShoppingCart find(int orderId) {
      ShoppingCart result = null;
      for (int i=0; i<orders.size() && result==null; i++) {
         ShoppingCart curCart = orders.get(i);
         if (curCart.getOrderId() == orderId) {
            result = curCart;
         }
      }
   
      return result;
   }
   
   /** finds all order with the given order statuses 
    *
    * @param states the list of values for orderStatus to look for
    * @return the ArrayList of ShoppingCart objects for this user with the given orderStatus values
    */
   public ArrayList<ShoppingCart> getOrdersWithStatus(String[] states) {
      ArrayList<ShoppingCart> carts = new ArrayList<ShoppingCart>();
      for (int i=0; i<orders.size(); i++) {
         ShoppingCart curCart = orders.get(i);
         for (int j=0; j<states.length; j++) {
            String status = states[j];
            if(curCart.getOrderStatus().equals(status)) {
               carts.add(curCart);
            }
         }
      }
      return carts;
   }
   
}