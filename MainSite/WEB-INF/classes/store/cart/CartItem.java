package store.cart;

/**
 * Model class to represent an item in a shopping cart
 * @author Clay Jacobs
 */
public class CartItem {
   /**
    * the item id
    */
   private int itemId;
   /**
    * the item name
    */
   private String itemName;
   /**
    * the item type (e.g. build, upgrade, etc.)
    */
   private String itemType;
   /**
    * the item description. format is "short-description;long-description"
    */
   private String itemDescription;
   /**
    * the cost of a single unit of this item
    */
   private double unitPrice;
   /**
    * the current number of this item that is in stock
    */
   private int maxInStock;
   
   /**
    * Empty constructor. Use setters to initialize the other properties
    */
   public CartItem(int id) {
      itemId = id;
      itemName = "";
      itemDescription = "";
      unitPrice=0.0;
      maxInStock = 0;
   }
   
   /**
    * Full constructor. Initialize all properties.
    */
   public CartItem(int id, String name, String type, String description, double price, int inventory) {
      itemId = id;
      itemName = name;
      itemType = type;
      itemDescription = description;
      unitPrice=price;
      maxInStock = inventory;
   }
   
   /** gets the item id 
    *
    * @return the itemId as an int
    */
   public int getItemId() {
      return itemId;
   }
   
   /** gets the item name 
    *
    * @return the itemName as a string
    */
   public String getName() {
      return itemName;
   }
   
   /** gets the item type 
    *
    * @return the itemType as a string 
    */
   public String getType() {
      return itemType;
   }
   
   /** gets the item description 
    *
    * @return the itemDescription as a string 
    */
   public String getDescription() {
      return itemDescription;
   }
   
   /** gets the item's unit price
    *
    * @return the unitPrice as a double
    */
   public double getPrice() {
      return unitPrice;
   }
   
   /** gets the stocked quantity of this item
    *
    * @return the maxInStock as an int
    */
   public int getInventory() {
      return maxInStock;
   }
   
   /** sets the item id
    *
    * @param id the int value to use for itemId
    */
   public void setItemId(int id) {
      itemId=id;
   }
   
   /** sets the item name
    *
    * @param name the string value to use for itemName
    */
   public void setName(String name) {
      itemName=name;
   }
   
   /** sets the item type
    *
    * @param type the string value to use for itemType
    */
   public void setType(String type) {
      itemType=type;
   }
   
   /** sets the item description
    *
    * @param description the string value to use for itemDescription
    */
   public void setDescription(String description) {
      itemDescription=description;
   }
   
   /** sets the item's unit price
    *
    * @param price the double value to use for unitPrice
    */
   public void setPrice(double price) {
      unitPrice=price;
   }
   
   /** sets the stocked quantity of this item
    *
    * @param inventory the int value to use for maxInStock
    */
   public void setInventory(int inventory) {
      maxInStock=inventory;
   }
   
   /** checks whether this item is stocked
    *
    * @return true if item is stocked. false otherwise
    */
   public boolean inStock() {
      return maxInStock!=0;
   }
   
}