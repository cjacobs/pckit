package store.cart;

import java.util.*;

import com.paypal.crypto.sample.ClientSide;
import java.security.cert.*;
import java.security.*;
import javax.crypto.*;
import org.bouncycastle.cms.*;
import org.bouncycastle.util.*;
import org.bouncycastle.cert.jcajce.*;
import org.bouncycastle.operator.*;
import org.bouncycastle.operator.jcajce.*;
import org.bouncycastle.cms.jcajce.*;
import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

//Code by Clay Jacobs
/**
 * Model class to represent a shopping cart
 * @author Clay Jacobs
 */
public class ShoppingCart {
   /**
    * the items in the shopping cart
    */
   private ArrayList<CartItem> items;
   /**
    * the quantities of the items in the shopping cart
    */
   private ArrayList<Integer> quantities;
   /**
    * the status of the order (e.g. "In Progress", "Buying", "Purchased")
    */
   private String orderStatus;
   /**
    * the id for this order
    */
   private int orderId;
   /**
    * the minimum build tier for this cart
    */
   private int minTier;
   
   /**
    * Constructor
    */
   public ShoppingCart() {
      items = new ArrayList<CartItem>();
      quantities = new ArrayList<Integer>();
      orderStatus = "";
   }
   
   /** gets the size of items ArrayList
    *
    * @return size of shopping cart arraylist
    */
   public int size() {
      return items.size();
   }
   
   /** add an item to the cart
    *
    * @param product the item to add to the cart
    * @param quantity the quantity of product to add to the cart
    */
   public void add(CartItem product, int quantity) {
      items.add(product);
      quantities.add(quantity);
   }
   
   /** remove an item from the cart
    *
    * @param index the index of the CartItem to remove
    */
   public void remove(int index) {
      items.remove(index);
      quantities.remove(index);
   }
   
   /** get a CartItem in this cart
    *
    * @param index the index of the CartItem to get
    * @return the CartItem at the specified index 
    */
   public CartItem get(int index) {
      return items.get(index);
   }
   
   /** set a CartItem in this cart
    *
    * @param index the index of the CartItem to aet
    * @param product the CartItem object to set at the specified index
    */
   public void set(int index, CartItem product) {
      items.set(index, product);
   }
   
   /** get the quantity of a CartItem in this cart
    *
    * @param index the index of the CartItem quantity to get
    * @return the quantity of the CartItem  at the specified index 
    */
   public int getItemQuantity(int index) {
      return quantities.get(index);
   }
   
   /** set the quantity of a CartItem in this cart
    *
    * @param index the index of the CartItem quantity to set
    * @param quantity the CartItem quantity to set at the specified index
    */
   public void setItemQuantity(int index, int quantity) {
      quantities.set(index, quantity);
   }
    
   /** get the index of a specified CartItem in cart
    *
    * @param buildId the id of the CartItem to search for in cart
    * @return the index of the CartItem in items 
    */
    public int find(int buildId) {
        int index=-1;
        int i=0;
        while (i<items.size() && index==-1) {
            CartItem curItem=items.get(i);
            if (curItem.getItemId()==buildId) {
                index=i;
            }
            i++;
        }
        return index;
    }
   
   /** get the total price of all items in cart
    *
    * @return the cost of this cart 
    */
   public double getTotalPrice() {
      double total=0;
      for (int i=0; i<items.size(); i++) {
         CartItem curItem = items.get(i);
         double itemCosts = curItem.getPrice() * quantities.get(i);
         total = total + itemCosts;
      }
      return total;
   }
   
   /** get the order id
    *
    * @return the orderId 
    */
   public int getOrderId() {
      return orderId;
   }
   
   /** set the order id
    *
    * @param id the value to use for orderId 
    */
   public void setOrderId(int id) {
      orderId = id;
   }
   
   /** get the order status
    *
    * @return the orderStatus 
    */
   public String getOrderStatus() {
      return orderStatus;
   }
   
   /** set the order status
    *
    * @param status the value to use for orderStatus 
    */
   public void setOrderStatus(String status) {
      orderStatus = status;
   }
   
   /** get the min tier
    *
    * @return the minTier
    */
   public int getMinTier() {
      return minTier;
   }
   
   /** set the min tier
    *
    * @param tier the value to use for minTier 
    */
   public void setMinTier(int tier) {
      minTier = tier;
   }
   
   /** 
    * @return the cart string for cookie storage. Not needed now.
    */
   public String getCookieStr() {
      String result = "";
      for (int i=0; i<items.size(); i++) {
         CartItem curItem = items.get(i);
         int quantity = quantities.get(i);
         if (i==items.size()-1) {
            result = result + curItem.getItemId() + ":" + quantity;
         }
         else {
            result = result + curItem.getItemId() + ":" + quantity + ",";
         }
      }
      return result;
   }
   
   /** 
    * @return the cart string for UI manipulation. Not needed now.
    */
   public String getDataStr() {
      String result = "";
      for (int i=0; i<items.size(); i++) {
         CartItem curItem = items.get(i);
         int quantity = quantities.get(i);
         if (i==items.size()-1) {
            result = result + curItem.getItemId() + ":" + quantity + ":" + curItem.getInventory() + ":" + curItem.getPrice()+  ":" + curItem.getName() ;
         }
         else {
            result = result + curItem.getItemId() + ":" + quantity + ":" + curItem.getInventory() + ":" + curItem.getPrice() +  ":" + curItem.getName() + ",";
         }
      }
      return result;
   }
   
   /** 
    * @return the encrypted content for the paypal checkout button.
    */
   public String getEncryptedStr() throws IOException, CertificateException, KeyStoreException, UnrecoverableKeyException,
	InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, CertStoreException, CMSException, OperatorCreationException{
      ClientSide client_side = new ClientSide( "my-pubcert2.pem", "my-prvkey2.p12", "paypal_cert_pem2.txt", "20BuildIt16" );
      
      //String basePath = "/Applications/tomcat/webapps/PCKitLive/OrderSection/";
      String basePath = "/home/ec2-user/pckit/MainSite/OrderSection/";
      String rawData = "cert_id=N497DGCVYQ89Y,cmd=_cart,upload=1,business=pckitcompany@gmail.com,custom="+orderId+",notify_url=https://www.pckit.org/OrderSection/ipn/,return=https://www.pckit.org/OrderSection/purchased.jsp,rm=2,cancel_return=https://www.pckit.org/OrderSection/canceled.jsp,";
      for (int i=0; i<items.size(); i++) {
         CartItem curItem = items.get(i);
         int quantity = quantities.get(i);
         int count = i+1;
         String itemName = curItem.getName().length() > 0 ? curItem.getName() : "PCKit Build " + curItem.getItemId();
         if (i==items.size()-1) {
            rawData = rawData + "item_name_" + count + "=" + itemName + ",amount_" + count + "=" + curItem.getPrice() + ",quantity_"  + count + "=" + quantity;
         }
         else {
            rawData = rawData + "item_name_" + count + "=" + itemName + ",amount_" + count + "=" + curItem.getPrice() + ",quantity_"  + count + "=" + quantity + ",";
         }
      }
      String result = client_side.getButtonEncryptionValue(rawData, basePath + "my-prvkey2.p12",  basePath + "my-pubcert2.pem", basePath +"paypal_cert_pem2.txt", "20BuildIt16" );
      return result;
   }
   
   
}