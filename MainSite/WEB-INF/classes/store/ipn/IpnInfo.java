/**
 * Paypal Button and Instant Payment Notification (IPN) Integration with Java
 */
package store.ipn;

/**
 * Model class to hold Paypal IPN Notification related information
 * @author Clay Jacobs
 */
 
 //Code by Clay Jacobs
public class IpnInfo {

    /**
     * the item name for the transaction from paypal
     */
    private String itemName;
    /**
     * the item number for the transaction from paypal
     */
    private String itemNumber;
    /**
     * the payment status for the transaction from paypal
     */
    private String paymentStatus;
    /**
     * the payment amount for the transaction from paypal
     */
    private String paymentAmount;
    /**
     * the payment currency for the transaction from paypal
     */
    private String paymentCurrency;
    /**
     * the transaction id for the transaction from paypal
     */
    private String txnId;
    /**
     * the receiver email for the transaction from paypal
     */
    private String receiverEmail;
    /**
     * the payer email for the transaction from paypal
     */
    private String payerEmail;
    /**
     * the response value for the transaction from paypal
     */
    private String response;
    /**
     * the request paramaters for the transaction from paypal
     */
    private String requestParams;
    /**
     * the error message for the transaction from paypal
     */
    private String error;
    /**
     * the time the notification was recieved from paypal
     */
    private Long logTime;
    //private int userId;
    /**
     * the order id from PCKit this IPN data pertains to
     */
    private int orderId;


    /** gets the order id 
     *
     * @return the orderId
     */
    public int getOrderId() {
       return orderId;
    }
    
    /** sets the order id 
     *
     * @param orderId the int value to use for orderId
     */
    public void setOrderId(int orderId) {
       this.orderId = orderId;
    }

    /** gets the item name 
     *
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /** sets the item name  
     *
     * @param itemName the string value to use for itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /** gets the item number 
     *
     * @return the itemNumber
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /** sets the item number  
     *
     * @param itemNumber the string value to use for itemNumber
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /** gets the log time 
     *
     * @return the logTime
     */
    public Long getLogTime() {
        return logTime;
    }

    /** sets the log time 
     *
     * @param logTime the Long value to use for logTime
     */
    public void setLogTime(Long logTime) {
        this.logTime = logTime;
    }

    /** gets the payer email
     *
     * @return the payerEmail
     */
    public String getPayerEmail() {
        return payerEmail;
    }

    /** sets the payer email
     *
     * @param payerEmail the string value to use for payerEmail
     */
    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    /** gets the payment amount
     *
     * @return the paymentAmount
     */
    public String getPaymentAmount() {
        return paymentAmount;
    }

    /** sets the payment amount
     *
     * @param paymentAmount the string value to use for paymentAmount
     */
    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    /** gets the payment currency
     *
     * @return the paymentCurrency
     */
    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    /** sets the payment currency
     *
     * @param paymentCurrency the string value to use for paymentCurrency
     */
    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    /** gets the payment status
     *
     * @return the paymentStatus
     */
    public String getPaymentStatus() {
        return paymentStatus;
    }

    /** sets the payment status
     *
     * @param paymentStatus the string value to use for paymentStatus
     */
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /** gets the receiver email
     *
     * @return the receiverEmail
     */
    public String getReceiverEmail() {
        return receiverEmail;
    }

    /** sets the receiver email
     *
     * @param receiverEmail the string value to use for receiverEmail
     */
    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    /** gets the request parameters string
     *
     * @return the requestParams
     */
    public String getRequestParams() {
        return requestParams;
    }

    /** sets the request parameters string
     *
     * @param requestParams the string value to use for requestParams
     */
    public void setRequestParams(String requestParams) {
        this.requestParams = requestParams;
    }

    /** gets the response string
     *
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /** sets the response string
     *
     * @param response the string value to use for response
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /** gets the transaction id string
     *
     * @return the txnId 
     */
    public String getTxnId() {
        return txnId;
    }

    /** sets the transaction id string
     *
     * @param txnId the string value to use for txnId
     */
    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    /** gets the error string
     *
     * @return the error 
     */
    public String getError() {
        return error;
    }

    /** sets the error string
     *
     * @param error the string value to use for error
     */
    public void setError(String error) {
        this.error = error;
    }

    /** gets the IpnInfo object as a string
     *
     * @return the string
     */
    @Override
    public String toString() {
        return "txn_id = " + this.getTxnId()
                 + ", orderId = " + this.getOrderId()
                + ", response = " + this.getResponse()
                + ", payment_status = " + this.getPaymentStatus()
                + ", payer_email = " + this.getPayerEmail()
                + ", item_name = " + this.getItemName()
                + ", item_number = " + this.getItemNumber()
                + ", payment_amount = " + this.getPaymentAmount()
                + ", payment_currency = " + this.getPaymentCurrency()
                + ", receiver_email = " + this.getReceiverEmail()
                + ", request_params = " + this.getRequestParams()
                + ", log_time = " + this.getLogTime()
                + ", error = " + this.getError();
    }

}