/**
 * Paypal Button and Instant Payment Notification (IPN) Integration with Java
 */
package store.ipn;

/**
 * This class serves as a model to hold Paypal IPN Integration Configuration
 *
 */
 
/** This class is used to store configuration info for the IPNListener servlet
 * @author Clay Jacobs
 */
 //Code by Clay Jacobs
public class IpnConfig {

    /**
     * the Paypal IPN URL
     */
    private String ipnUrl;
    /**
     * the receiver email
     */
    private String receiverEmail;
    /**
     * the payment amount
     */
    private String paymentAmount;
    /**
     * the payment currency
     */
    private String paymentCurrency;


    /** 
     * Constructor 
     */
    public IpnConfig (String ipnUrl, String receiverEmail, String paymentAmount, String paymentCurrency) {
        this.ipnUrl = ipnUrl;
        this.receiverEmail = receiverEmail;
        this.paymentAmount = paymentAmount;
        this.paymentCurrency = paymentCurrency;
    }

    /** gets the Paypal IPN URL
     *
     * @return the ipnUrl
     */
    public String getIpnUrl() {
        return ipnUrl;
    }

    /** sets the Paypal IPN URL
     *
     * @param ipnUrl the value to use for ipnUrl
     */
    public void setIpnUrl(String ipnUrl) {
        this.ipnUrl = ipnUrl;
    }

    /** gets the payment amount string
     *
     * @return the paymentAmount
     */
    public String getPaymentAmount() {
        return paymentAmount;
    }

    /** sets the payment amount string
     *
     * @param paymentAmount the string value to use for paymentAmount
     */
    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    /** gets the payment currency
     *
     * @return the paymentCurrency
     */
    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    /** sets the payment currency string
     *
     * @param paymentCurrency the string value to use for paymentCurrency
     */
    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    /** gets the receiver email
     *
     * @return the receiverEmail
     */
    public String getReceiverEmail() {
        return receiverEmail;
    }

    /** sets the receiver email string
     *
     * @param receiverEmail the string value to use for receiverEmail
     */
    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

}
